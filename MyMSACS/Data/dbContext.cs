﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MyMSACS.Models;

namespace MyMSACS.Data
{
    public class dbContext : DbContext
    {
        public dbContext(DbContextOptions<dbContext> options) : base(options)
        {
        }

        public DbSet<Degree> degree { get; set; }
        public DbSet<StudentDegreePlan> student_degree_plan { get; set; }
        public DbSet<DegreeRequirement> degreeRequirement { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<PlanTerm> planRequirement { get; set; }
        public DbSet<StudentDegreePlan> studentDegreePlan { get; set; }
        public DbSet<PlanTermRequirement> termRequirement { get; set; }
        public DbSet<RequirementStatus> requirementStatus { get; set; }
        public DbSet<DegreeStatus> degreeStatus { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Degree>().ToTable("Course");
            modelBuilder.Entity<DegreeRequirement>().ToTable("Enrollment");
            modelBuilder.Entity<Student>().ToTable("Student");
            modelBuilder.Entity<PlanTerm>().ToTable("PlanTerm");
            modelBuilder.Entity<StudentDegreePlan>().ToTable("StudentDegreePlan");
            modelBuilder.Entity<PlanTermRequirement>().ToTable("TermRequirement");
        }
    }
}
