﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyMSACS.Models
{
    public class PlanTermRequirement
    {
        public int PlanTermRequirementID { get; internal set; }
        public int StudentID { get; internal set; }
        public int DegreeID { get; internal set; }
        public int PlanNumber { get; internal set; }
        public int TermNumber { get; internal set; }
        public int RequirementNumber { get; internal set; }
    }
}
