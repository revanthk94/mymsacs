﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyMSACS.Models
{
    public class RequirementStatus
    {
        public int RequirementStatusID { get; internal set; }
        public string Requirement_Status { get; internal set; }
        //testing
    }
}