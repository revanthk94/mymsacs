using System.Linq;
using System.Threading.Tasks;

namespace MyMSACS.Models
{
    public class Course
    {
        public int courseId { get; set; }
        public string courseName { get; set; }
        public string facultyName { get; set; }

    }
}
