﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyMSACS.Models
{
    public class Student
    {
        public int StudentID { get; internal set; }
        public string GivenName { get; internal set; }
        public string FamilyName { get; internal set; }
    }
}
