﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyMSACS.Models
{
    public class DegreeRequirement
    {
        public int DegreeRequirementID { get; set; }
        public int RequirementNumber { get; set; }
        public int DegreeID { get; set; }
        public string RequirementAbbrev { get; set; }
        public string RequirementName { get; set; }
        public string lkDegree { get; set; }
        
    }
}
