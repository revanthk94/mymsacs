﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyMSACS.Models
{
    public class StudentDegreePlan
    {/*
        public int studentdegreeId { get; set; }
        public string StudentdegreeName { get; set; }
        public int CreditHrsrequired { get; set; }
        */
        public int StudentDegreePlanID { get; internal set; }
        public int StudentID { get; internal set; }
        public int DegreeID { get; internal set; }
        public int PlanNumber { get; internal set; }
        public string PlanAbbrev { get; internal set; }
        public string PlanName { get; internal set; }
        public DateTime CreateDate { get; internal set; }
        public DateTime EditDate { get; internal set; }
        public int DegreeStatusID { get; internal set; }
    }
}
