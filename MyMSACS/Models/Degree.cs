﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyMSACS.Models
{
    public class Degree
    {
        public int DegreeID { get; set; }
        public string DegreeAbbrev { get; set; }
        public string DegreeName { get; set; }

    }
}
