﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyMSACS.Models
{
    public class DegreeStatus
    {
        public int DegreeStatusID { get; internal set; }
        public string Degree_Status { get; internal set; }
    }
}